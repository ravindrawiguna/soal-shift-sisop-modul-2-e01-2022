#include <limits.h>
#include <wait.h>
#include <time.h>
#include <pwd.h>//get home dir stuff
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include<unistd.h> 

void waitChildToDie(){
  int status;
  while(wait(&status) > 0);
}

void combineTwoString(char dst[], char str1[], char str2[]){
  strcpy(dst, str1);
  strcat(dst, str2);
}

void reverseString(char dst[], char src[]){
  int len_src = strlen(src);
  int id = 0;
  for(int i = len_src-1;i>=0;i--){
    dst[id] = src[i];
    id+=1; 
  }
  dst[id] = 0;
}

void intToString(int n, char str[], int format){
// 2147483647 10 , so uh  12
  char temp[12];
  int id = 0;
  int savedn = n;
  while(n > 0){
    char digit = n%10 + '0';
    n = n/10;
    temp[id] = digit;
    id+=1;
  }
  while(id < format){
    temp[id] = '0';
    id+=1;
  }
  temp[id] = 0;//teruminate
  reverseString(str, temp);
}
struct tm getDateTime(){
  time_t t = time(0);
  struct tm tmvar = *localtime(&t);
  return tmvar;
}

int checkDateTime(struct tm cur_time, int year, int month, int mday, int hour, int min){
  // printf("called\n");
  int diffY = cur_time.tm_year - year;
  if(diffY < -1900){
    // printf("tada");
    // printf("%d\n", diffY);
    return -1;//hm year is smoler
  }
  if(diffY > 0){
    // printf("tidi");
    return 1; //year is bigger
  }
  //year is equal
  int diffM = cur_time.tm_mon - month;
  if(diffM < -1){
    // printf("tudu");
    // printf("%d\n", diffM);
    return -1;
  }
  if(diffM > 0){
    // printf("tede");
    return 1;
  }

  int diffMday = cur_time.tm_mday - mday;
  if(diffMday < 0){
    // printf("todo");
    return -1;
  }
  if(diffMday > 0){
    // printf("daya");
    return 1;
  }

  int diffH = cur_time.tm_hour - hour;
  if(diffH < 0){
    // printf("diyi");
    return -1;
  }
  if(diffH > 0){
    // printf("duyu");
    return 1;
  }
  int diffMin = cur_time.tm_min - min;
  if(diffMin < 0){
    // printf("deye");
    return -1;
  }
  if(diffMin >= 0){
    // printf("doyo");
    return 1;// i mean last so just tell it 1
  }
  // printf("should not be here\n");
}

int getRandomNumber(int max_num){
  int lucky_number = rand() % max_num;
  return lucky_number;
}

void checkDirectory(){
   char cwd[PATH_MAX];
   if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("The working is this dir sir: %s\n", cwd);
   } else {
       perror("getcwd() error");
   }  
}

void downloadFile(char url[], char location[]){
  pid_t childAbuse_download = fork();
  if(childAbuse_download < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_download == 0){
    //chid
    execlp("wget", "wget", "--no-check-certificate", url, "-O", location, "-q", NULL);
  }
  else{
    waitChildToDie();
  }
}

void createDir(char directoryName[]){
  // mengefork to create dir
  pid_t childAbuse_mkdirId = fork();
  if(childAbuse_mkdirId < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_mkdirId == 0){
    // li shi bru, we are in child
    execlp("mkdir", "mkdir","-p",directoryName, NULL);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void unzipit(char fileLocation[], char savedLoc[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-qq",fileLocation, "-d", savedLoc,NULL);
  }
  else{
    waitChildToDie();
  }
}

void deleteDir(char dirRelativePath[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("rm", "rm",  "-rf",dirRelativePath,NULL);
    }
    else{
        waitChildToDie();
    }
}

void copyFile(char filePath[], char destination[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("cp", "cp",filePath, destination,NULL);
    }
    else{
        waitChildToDie();
    }
}

void deleteFile(char filePath[]){
    pid_t childAbuse_del = fork();
    if(childAbuse_del < 0){
        exit(EXIT_FAILURE);
    }
    if(childAbuse_del == 0){
        execlp("rm", "rm",filePath,NULL);
    }
    else{
        waitChildToDie();
    }
}

void writeTextFile(char textFilePath[], char input[]){
    FILE *fptr;
    int len = strlen(input);
    fptr = fopen(textFilePath,"a");//append sapa tau mau nambah
    if(input[len-1]=='\n'){
        fprintf(fptr,"%s",input);
    }else{
        fprintf(fptr,"%s\n",input);
    }
    
    fclose(fptr);
}

void parsePhoto(char photo_name[256], char thepath[PATH_MAX]){
    // create photo path
    char *photoPath;
    photoPath = malloc(sizeof(char)*256);
    combineTwoString(photoPath, thepath, photo_name);
    // variable for parsing
    int isParsing = 1;
    char *filmName, *filmCategory, *filmYear;
    int len = strlen(photo_name);
    int i = 0;
    while(isParsing){
        int isGetName = 1;
        int isGetYear = 0;
        int isGetCategory = 0;
        filmName = malloc(sizeof(char)*128);
        filmCategory = malloc(sizeof(char)*128);
        filmYear = malloc(sizeof(char)*8);
        
        int idName = 0;
        // get name
        for(i = i;i < len && isGetName;i++){
            char c = photo_name[i];
            if(c == ';'){
                isGetName = 0;
                isGetYear = 1;
                filmName[idName] = 0;
            }else{
                filmName[idName] = photo_name[i];
                idName+=1;
            }
        }
        // get year
        int idYear = 0;
        for(i = i;i<len && isGetYear;i++){
            char c = photo_name[i];
            if(c == ';'){
                isGetYear = 0;
                isGetCategory = 1;
                filmYear[idYear] = 0;
            }else{
                filmYear[idYear] = c;
                idYear +=1;
            }
        }
        // get categry
        int idCat = 0;
        for(i = i;i<len && isGetCategory;i++){
            char c = photo_name[i];
            if(c == '_' || c == '.'){
                isGetCategory = 0;
                // filmCategory[idCat] = '/';
                filmCategory[idCat] = 0;
            }else{
                filmCategory[idCat] = c;
                idCat +=1;
            }
        }
        // now let see is there another
        isParsing = photo_name[i-1] == '_';
        // i+= isParsing;
        // save doeloe
        // create dir
        char *categoryPath;
        categoryPath = malloc(sizeof(char)*256);
        combineTwoString(categoryPath, thepath, filmCategory);
        strcat(categoryPath, "/");
        createDir(categoryPath);
        
        // after that we uh save the photo
        // copy then remove
        char *tempPhotoLoc;
        tempPhotoLoc = malloc(sizeof(char)*256);
        combineTwoString(tempPhotoLoc, categoryPath, filmName);
        char *truePhotoLoc;
        truePhotoLoc = malloc(sizeof(char)*256);
        strcpy(truePhotoLoc, tempPhotoLoc);
        strcat(truePhotoLoc, ".png");
        copyFile(photoPath, truePhotoLoc);
        // printf("film name: %s\nfilm year: %s\nfilm cat: %s\ncategory Path: %s\nnew Photo Path: %s\n", filmName, filmYear, filmCategory, categoryPath, newPhotoLoc);
        
        // save the year on txt
        char *textFilePath;
        textFilePath = malloc(sizeof(char)*256);
        strcpy(textFilePath, tempPhotoLoc);
        strcat(textFilePath, ".txt");
        // printf("%s\n", textFilePath);
        writeTextFile(textFilePath, filmName);
        writeTextFile(textFilePath, filmYear);
        free(tempPhotoLoc);free(categoryPath);free(truePhotoLoc);free(textFilePath);
    }
    // remove the photo
    deleteFile(photoPath);
    free(filmCategory);free(filmName);free(filmYear);free(photoPath);
}

void filterFileDir(char thepath[PATH_MAX]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
            // not . or ..
            // printf("====%s====\n", ep->d_name);
            // check string belakang
            int len = strlen(ep->d_name);
            // printf("%d\n", len);
            // printf("%c\n", ep->d_name[len-1]);
            char format[4] = "gnp.";
            int isFolder = 4;
            for(int i = 0;i<4;i++){
                // printf("%c==%c\n",ep->d_name[len-i-1],format[i] );
                isFolder -= ep->d_name[len-i-1]==format[i];
            }
            if(isFolder){
                // printf("%s is folder\n", ep->d_name);
                char *folderLoc;
                folderLoc = malloc(sizeof(char)*256);
                combineTwoString(folderLoc, thepath, ep->d_name);
                // printf("%s\n", folderLoc);
                deleteDir(folderLoc);
                free(folderLoc);
            }else{
                char *fileLoc;
                fileLoc = malloc(sizeof(char)*256);
                combineTwoString(fileLoc, thepath, ep->d_name);
                // printf("%s\n", folderLoc);
                // deleteDir(folderLoc);
                parsePhoto(ep->d_name, thepath);
                free(fileLoc);
                // printf("%s is not a folder\n", ep->d_name);
                // parse poster name
            }
            
        }
    }

    (void) closedir (dp);
  } else perror ("Couldn't open the directory");

}

typedef struct
{
    /* data */
    char name[256];
    char year[8];
}movie;


void readTextFile(char textFilePath[], movie *m){
    FILE *fp = fopen(textFilePath, "r");

    if (fp == NULL)
    {
        printf("Error: could not open file %s", textFilePath);
        return;
    }

    // reading line by line, max 256 bytes
    const unsigned MAX_LENGTH = 256;
    char *buffer;
    buffer = malloc(sizeof(char)*MAX_LENGTH);

    fgets(buffer, MAX_LENGTH, fp);
    // printf("%s", buffer);
    strcpy(m->name, buffer);

    fgets(buffer, MAX_LENGTH, fp);
    // printf("%s", buffer);
    // char c;
    // scanf("%c\n", &c);
    strcpy(m->year, buffer);
    free(buffer);
    // close the file
    fclose(fp);
}


void printMovie(movie m){
    printf("name: %s\nyear: %s", m.name, m.year);
}
 
// A function to implement bubble sort
void bubbleSort(movie arr[], int size)
{
    for(int i = 0;i<size;i++){
        for(int j = 0;j<size-i-1;j++){
            // printMovie(arr[i]);
            if(strcmp(arr[j].name, arr[j+1].name) > 0){
                // swap(&arr[j], &arr[j+1]);
                movie temp = arr[j];

                memset(arr[j].name, 0, sizeof(char)*256);
                strcpy(arr[j].name, arr[j+1].name);
                memset(arr[j].year, 0, sizeof(char)*8);
                strcpy(arr[j].year, arr[j+1].year);

                memset(arr[j+1].name, 0, sizeof(char)*256);
                strcpy(arr[j+1].name, temp.name);
                memset(arr[j+1].year, 0, sizeof(char)*8);
                strcpy(arr[j+1].year, temp.year);
            }
        }
    }
}

void compileDataCategory(char thepath[PATH_MAX], char dataFilePath[]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    movie arr_mov[16];
    int idMov = 0;
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
            // not . or ..
            int len = strlen(ep->d_name);
            char format[4] = "txt.";
            int isPng = 4;
            for(int i = 0;i<4;i++){
                // printf("%c==%c\n",ep->d_name[len-i-1],format[i] );
                isPng -= ep->d_name[len-i-1]==format[i];
            }
            
            if(isPng){
                continue;
            }
            // a txt, eh cek dia data tida
            if(strcmp(ep->d_name, "data.txt")==0){
                // skip
                continue;
            }
            char *txtFilePath = malloc(sizeof(char)*256);
            combineTwoString(txtFilePath, thepath, ep->d_name);

            readTextFile(txtFilePath, &arr_mov[idMov]);
            idMov+=1;
            // delete it dud
            deleteFile(txtFilePath);

            free(txtFilePath);
   
            
        }

    }
    // sort it
    bubbleSort(arr_mov, idMov);
    // masukin ke txt
    for(int i = 0;i<idMov;i++){
        char *inputName = malloc(sizeof(char)*256);
        char *inputYear = malloc(sizeof(char)*256);
        combineTwoString(inputName, "nama: ", arr_mov[i].name);
        combineTwoString(inputYear, "rilis : tahun ", arr_mov[i].year);
        writeTextFile(dataFilePath, inputName);
        writeTextFile(dataFilePath, inputYear);
        writeTextFile(dataFilePath, "\n");
        free(inputName);free(inputYear);
    }
    (void) closedir (dp);
  } else perror ("Couldn't open the directory");
  
}

void createDataCategory(char thepath[PATH_MAX]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    // create data.txt inside
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
            // not . or ..
            int len = strlen(ep->d_name);
            char *categoryPath, *dataCatPath;
            categoryPath = malloc(sizeof(char)*256);
            dataCatPath = malloc(sizeof(char)*256);
            combineTwoString(categoryPath, thepath, ep->d_name);
            strcat(categoryPath, "/");
            combineTwoString(dataCatPath, categoryPath, "data.txt");
            char *header = malloc(sizeof(char)*256);
            combineTwoString(header, "kategori: ", ep->d_name);
            writeTextFile(dataCatPath, header);
            writeTextFile(dataCatPath, "\n");
            compileDataCategory(categoryPath, dataCatPath);
            free(categoryPath);free(dataCatPath);free(header);

            
        }
    }

    (void) closedir (dp);
  } else perror ("Couldn't open the directory");

}

//=================================================================================
int main(int argc, char** argv) {
    //get user homedir of whoever running this program
    char *homedir;
    // check ada variable home kah di enviromentnya
    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    char root[100] = "";
    combineTwoString(root, homedir, "/shift2/"); //this is where we do our work
    // printf("%s\n", root);
    if ((chdir(root)) < 0) {
        printf("FAIL\n");
        exit(EXIT_FAILURE);
    }

    //  extract drakor.zip
    char savedLoc[256];
    combineTwoString(savedLoc, root, "drakor/");
    // printf("%s\n", savedLoc);
    unzipit("drakor.zip", savedLoc);
    // delete folder, extract category, rename and copy picture
    filterFileDir(savedLoc);
    // create data txt
    createDataCategory(savedLoc);
    return 0;
}