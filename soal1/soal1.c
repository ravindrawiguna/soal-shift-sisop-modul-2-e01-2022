#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

#include <limits.h>
#include<json-c/json.h>
#include <pwd.h>//for pwd stuff
#include <wait.h>
#include <time.h>
#include <dirent.h>

void waitChildToDie(){
  int status;
  while(wait(&status) > 0);
}

void combineTwoString(char dst[], char str1[], char str2[]){
  strcpy(dst, str1);
  strcat(dst, str2);
}

void reverseString(char dst[], char src[]){
  int len_src = strlen(src);
  int id = 0;
  for(int i = len_src-1;i>=0;i--){
    dst[id] = src[i];
    id+=1; 
  }
  dst[id] = 0;
}

void intToString(int n, char str[], int format){
// 2147483647 10 , so uh  12
  char temp[12];
  int id = 0;
  int savedn = n;
  while(n > 0){
    char digit = n%10 + '0';
    n = n/10;
    temp[id] = digit;
    id+=1;
  }
  while(id < format){
    temp[id] = '0';
    id+=1;
  }
  temp[id] = 0;//teruminate
  reverseString(str, temp);
  printf("uh, str: %s from %d\nehe\n", str, savedn);
}
struct tm getDateTime(){
  time_t t = time(0);
  struct tm tmvar = *localtime(&t);
  return tmvar;
}

int checkDateTime(struct tm cur_time, int year, int month, int mday, int hour, int min){
  // printf("called\n");
  int diffY = cur_time.tm_year - year;
  if(diffY < -1900){
    // printf("tada");
    // printf("%d\n", diffY);
    return -1;//hm year is smoler
  }
  if(diffY > 0){
    // printf("tidi");
    return 1; //year is bigger
  }
  //year is equal
  int diffM = cur_time.tm_mon - month;
  if(diffM < -1){
    // printf("tudu");
    // printf("%d\n", diffM);
    return -1;
  }
  if(diffM > 0){
    // printf("tede");
    return 1;
  }

  int diffMday = cur_time.tm_mday - mday;
  if(diffMday < 0){
    // printf("todo");
    return -1;
  }
  if(diffMday > 0){
    // printf("daya");
    return 1;
  }

  int diffH = cur_time.tm_hour - hour;
  if(diffH < 0){
    // printf("diyi");
    return -1;
  }
  if(diffH > 0){
    // printf("duyu");
    return 1;
  }
  int diffMin = cur_time.tm_min - min;
  if(diffMin < 0){
    // printf("deye");
    return -1;
  }
  if(diffMin >= 0){
    // printf("doyo");
    return 1;// i mean last so just tell it 1
  }
  // printf("should not be here\n");
}

int getRandomNumber(int max_num){
  int lucky_number = rand() % max_num;
  return lucky_number;
}

void checkDirectory(){
   char cwd[PATH_MAX];
   if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("The working is this dir sir: %s\n", cwd);
   } else {
       perror("getcwd() error");
   }  
}

void downloadFile(char url[], char location[]){
  pid_t childAbuse_download = fork();
  if(childAbuse_download < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_download == 0){
    //chid
    execlp("wget", "wget", "--no-check-certificate", url, "-O", location, "-q", NULL);
  }
  else{
    waitChildToDie();
  }
}

void createDir(char *directoryName[]){
  // mengefork to create dir
  pid_t childAbuse_mkdirId = fork();
  if(childAbuse_mkdirId < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_mkdirId == 0){
    // li shi bru, we are in child
    execvp("mkdir", directoryName);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void unzipit(char fileLocation[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-qq",fileLocation,NULL);
  }
  else{
    waitChildToDie();
  }
}

void extractFileNameInDir(char thepath[PATH_MAX], char extractedName[250][250], int *totalDiscovered){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  int idExtract = 0;
  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
          strcpy(extractedName[idExtract], ep->d_name);
          idExtract+=1;
        }
    }

    (void) closedir (dp);
    printf("There are %d files in %s\n", idExtract, thepath);
  } else perror ("Couldn't open the directory");

  *totalDiscovered = idExtract;
}

void zipFolder(){
  pid_t childAbuse_zip = fork();
  if(childAbuse_zip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_zip == 0){
    execlp("zip", "zip",  "-rP", "satuduatiga","not_safe_for_wibu.zip", "gacha_gacha", NULL);
  }
  else{
    waitChildToDie();
  }
  
}

void removeDir(){
  pid_t childAbuse_rm = fork();
  if(childAbuse_rm < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_rm == 0){
    // li shi bru, we are in child
    execlp("rm", "rm", "-rf", "gacha_gacha", NULL);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void initialize(char fullPathWibu[]){
  // Pertama kali jalan 
  char weaponUrl[100] = "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
  char weaponName[100] = "weapons.zip";
  char weaponLocation[100] ="";
  combineTwoString(weaponLocation, fullPathWibu, weaponName);

  char charaterUrl[100] = "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
  char charaterName[100] ="charaters.zip";
  char characterLocation[100] = "";
  combineTwoString(characterLocation, fullPathWibu, charaterName);
  
  downloadFile(weaponUrl, weaponLocation);
  // waitChildToDie();
  sleep(1);
  downloadFile(charaterUrl, characterLocation);
  // waitChildToDie();
  sleep(1);
  unzipit(characterLocation);
  // waitChildToDie();
  unzipit(weaponLocation);
  // waitChildToDie();
}

void parseJsonRarityName(char fileName[], char nameChar[], char rarityChar[]){
    // printf("calledddddddd\n");
    char *buffer;
    buffer  = malloc(sizeof(char)*16384);

    FILE *fp = fopen(fileName,"r");
    // printf("readin\n");
    fread(buffer, 16384, 1, fp);
    // printf("closin %d\n", res);
    fclose(fp);
    // printf("mamamia lezatos\n");
    struct json_object *parsed_json;
    parsed_json = json_tokener_parse(buffer);
    // printf("ulala syre\n");
    struct json_object *rarity;
    struct json_object *name;
    // printf("naa wae it stop here\n");
    json_object_object_get_ex(parsed_json, "name", &name);
    // printf("ngekstrak name\n");
    json_object_object_get_ex(parsed_json, "rarity", &rarity);


    strcpy(nameChar, json_object_get_string(name));
    strcpy(rarityChar, json_object_get_string(rarity));

    free(buffer);
}

void updateGachaFolderName(char *currentFolderName, int iterGacha){
  // reset the current folder array
  memset(currentFolderName, 0, sizeof(char)*256);
  // get str version of cur iter gacha
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFolderName, "gacha_gacha/total_gacha_");
  strcat(currentFolderName, iterStrGacha);
  strcat(currentFolderName, "/");
  char *command[] = {"-p", currentFolderName, NULL};
  createDir(command);
  free(iterStrGacha);
}

void updateGachaFileName(char *currentFileName, char *currentFolderName, struct tm tmv, int iterGacha){
  memset(currentFileName, 0, sizeof(char)*256);
  //get hour
  char *hours, *minutes, *seconds;
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  hours = malloc(sizeof(char)*4);
  minutes = malloc(sizeof(char)*4);
  seconds = malloc(sizeof(char)*4);
  intToString(tmv.tm_hour, hours, 2);
  intToString(tmv.tm_min, minutes, 2);
  intToString(tmv.tm_sec, seconds, 2);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFileName, currentFolderName);
  // strcat(currentFileName, iterStrGacha);
  // strcat(currentFileName, "/");
  strcat(currentFileName, hours);
  strcat(currentFileName, ":");
  strcat(currentFileName, minutes);
  strcat(currentFileName, ":");
  strcat(currentFileName, seconds);
  strcat(currentFileName, "_gacha_");

  
  strcat(currentFileName, iterStrGacha);
  strcat(currentFileName, ".txt");
  free(hours);free(minutes);free(seconds);free(iterStrGacha);
}


//=================================================================================
int main(int argc, char** argv) {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  //get user homedir of whoever running this program
  char *homedir;
  // check ada variable home kah di enviromentnya
  if ((homedir = getenv("HOME")) == NULL) {
    homedir = getpwuid(getuid())->pw_dir;
  }

  char root[100] = "";
  combineTwoString(root, homedir, "/soal1modul2/"); //this is where we do our work
  // printf("%s\n", root);
  if ((chdir(root)) < 0) {
    printf("FAIL\n");
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  //================================================START================
  // membuat folder gacha gacha di "root"
  char pathGacha[PATH_MAX] = "";
  combineTwoString(pathGacha, root, "gacha_gacha");
  char *gachaName[] = {"-p", pathGacha, NULL};
  createDir(gachaName);

  initialize(root);
  // printf("DONE\n");
  
  srand(time(0));
  char charName[] = "characters";
  char listChar[250][250];
  int totalChar = 0;
  extractFileNameInDir(charName, listChar, &totalChar);

  int totalWeapon = 0;
  char listWeapon[250][250];
  extractFileNameInDir("weapons", listWeapon, &totalWeapon);

  // get date time
  struct tm tmv = getDateTime();
  // loop sampek 30 maret 2022 04:44
  int isWaiting = 0;
  while(checkDateTime(tmv, 2022, 3, 30, 4, 44) < 0){
    // isWaiting = ;
    printf("waiting: %d", isWaiting);
    tmv = getDateTime();
    printf("now: %d-%02d-%02d %02d:%02d:%02d\n", tmv.tm_year + 1900, tmv.tm_mon + 1, tmv.tm_mday, tmv.tm_hour, tmv.tm_min, tmv.tm_sec);
  }
  // sudah, start gacha
  // variable important for gacha
 // untuk gacha stuff
  int iterGacha = 1;
  int primogems = 79000;
  // FILE *fptr;
  char *currentFileName, *currentFolderName;
  currentFileName = malloc(sizeof(char)*256);
  currentFolderName = malloc(sizeof(char)*256);
  updateGachaFolderName(currentFolderName, iterGacha);
  updateGachaFileName(currentFileName, currentFolderName,tmv, iterGacha); 
  // char digits[12];
  // intToString(123456, digits);
  int isRunning = 1;
  while (isRunning) {
    tmv = getDateTime();
    // ceku taimu
    if(checkDateTime(tmv, 2022, 3, 30, 7, 44) > 0){
      isRunning=0;
      zipFolder();
      removeDir();
      break;
    }
    // Tulis program kalian di sini
    
    // gacha wepon or char
    if(iterGacha&1){
      // this is ganjil ==  character
      //masukin gacha ke txt dengan format 
      //{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}
      int idGachaChar = getRandomNumber(totalChar);
      // printf("randomo aidi: %d\n", idGachaChar);
      char jsonFileName[250];
      char charName[250];
      char rarityChar[3];
      // save the picked random char json file name
      combineTwoString(jsonFileName, "characters/", listChar[idGachaChar]);


      parseJsonRarityName(jsonFileName, charName, rarityChar);

      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_character_%s_%s_%d\n",iterGacha, rarityChar, charName, primogems);
      fclose(fptr);
      printf("saved character on %s\n", currentFileName);
    }else{
      printf("it deez not\n");
      int idGachaChar1 = getRandomNumber(totalWeapon);
      char jsonFileName1[250];
      char charName1[250];
      char rarityChar1[3];

      // save the picked random char json file name
      combineTwoString(jsonFileName1, "weapons/", listWeapon[idGachaChar1]);

      parseJsonRarityName(jsonFileName1, charName1, rarityChar1);

      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_weapon_%s_%s_%d\n",iterGacha, rarityChar1, charName1, primogems);
      fclose(fptr);//ensure every open is lsg close
      printf("saved weapon on %s\n", currentFileName);
    }
    // update new folder or no
    if(iterGacha%90 == 0){
      updateGachaFolderName(currentFolderName, iterGacha);
    }
    // new file or no
    if(iterGacha%10 == 0){
      //update file
      updateGachaFileName(currentFileName, currentFolderName,tmv, iterGacha);

    }
    iterGacha+=1;
    sleep(1);
  }
  return 0;
}