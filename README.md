# soal-shift-sisop-modul-2-E01-2022

Laporan soal dan penyelesaian untuk modul 2 mata kuliah sistem operasi kelas E tahun 2022


## DAFTAR ISI ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomor 1](#nomor-1)
    - [Soal  1.a](#soal-1a)
    - [Soal  1.b](#soal-1b)
    - [Soal  1.c](#soal-1c)
    - [Soal  1.d](#soal-1d)
    - [Soal  1.e](#soal-1e)
- [Nomor 2](#nomor-2)
    - [Soal  2.a](#soal-2a)
    - [Soal  2.b](#soal-2b)
    - [Soal  2.c](#soal-2c)
    - [Soal  2.d](#soal-2d)
    - [Soal  2.e](#soal-2e)
- [Nomor 3](#nomor-3)
    - [Soal  3.a](#soal-3a)
    - [Soal  3.b](#soal-3b)
    - [Soal  3.c](#soal-3c)
    - [Soal  3.d](#soal-3d)
    - [Soal  3.e](#soal-3e)
- [Dokumentasi](#dokumentasi)
## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201237  | Putu Ravindra Wiguna | SISOP E
5025201003    | Rahmat Faris Akbar | SISOP E
05111740000146    | Muhammad Kiantaqwa Farhan | SISOP E

## Nomor 1 ##
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

### Soal 1.a ###
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

#### Dokumentasi Source Code ####
FUNGSI-FUNGSI HELPER
```bash

void waitChildToDie(){
  int status;
  while(wait(&status) > 0);
}

void combineTwoString(char dst[], char str1[], char str2[]){
  strcpy(dst, str1);
  strcat(dst, str2);
}

void reverseString(char dst[], char src[]){
  int len_src = strlen(src);
  int id = 0;
  for(int i = len_src-1;i>=0;i--){
    dst[id] = src[i];
    id+=1; 
  }
  dst[id] = 0;
}

void intToString(int n, char str[], int format){
// 2147483647 10 , so uh  12
  char temp[12];
  int id = 0;
  int savedn = n;
  while(n > 0){
    char digit = n%10 + '0';
    n = n/10;
    temp[id] = digit;
    id+=1;
  }
  while(id < format){
    temp[id] = '0';
    id+=1;
  }
  temp[id] = 0;//teruminate
  reverseString(str, temp);
  printf("uh, str: %s from %d\nehe\n", str, savedn);
}
struct tm getDateTime(){
  time_t t = time(0);
  struct tm tmvar = *localtime(&t);
  return tmvar;
}

int checkDateTime(struct tm cur_time, int year, int month, int mday, int hour, int min){
  // printf("called\n");
  int diffY = cur_time.tm_year - year;
  if(diffY < -1900){
    // printf("tada");
    // printf("%d\n", diffY);
    return -1;//hm year is smoler
  }
  if(diffY > 0){
    // printf("tidi");
    return 1; //year is bigger
  }
  //year is equal
  int diffM = cur_time.tm_mon - month;
  if(diffM < -1){
    // printf("tudu");
    // printf("%d\n", diffM);
    return -1;
  }
  if(diffM > 0){
    // printf("tede");
    return 1;
  }

  int diffMday = cur_time.tm_mday - mday;
  if(diffMday < 0){
    // printf("todo");
    return -1;
  }
  if(diffMday > 0){
    // printf("daya");
    return 1;
  }

  int diffH = cur_time.tm_hour - hour;
  if(diffH < 0){
    // printf("diyi");
    return -1;
  }
  if(diffH > 0){
    // printf("duyu");
    return 1;
  }
  int diffMin = cur_time.tm_min - min;
  if(diffMin < 0){
    // printf("deye");
    return -1;
  }
  if(diffMin >= 0){
    // printf("doyo");
    return 1;// i mean last so just tell it 1
  }
  // printf("should not be here\n");
}

int getRandomNumber(int max_num){
  int lucky_number = rand() % max_num;
  return lucky_number;
}

void checkDirectory(){
   char cwd[PATH_MAX];
   if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("The working is this dir sir: %s\n", cwd);
   } else {
       perror("getcwd() error");
   }  
}

void downloadFile(char url[], char location[]){
  pid_t childAbuse_download = fork();
  if(childAbuse_download < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_download == 0){
    //chid
    execlp("wget", "wget", "--no-check-certificate", url, "-O", location, "-q", NULL);
  }
  else{
    waitChildToDie();
  }
}

void createDir(char *directoryName[]){
  // mengefork to create dir
  pid_t childAbuse_mkdirId = fork();
  if(childAbuse_mkdirId < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_mkdirId == 0){
    // li shi bru, we are in child
    execvp("mkdir", directoryName);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void unzipit(char fileLocation[]){
  pid_t childAbuse_unzip = fork();
  if(childAbuse_unzip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_unzip == 0){
    execlp("unzip", "unzip",  "-qq",fileLocation,NULL);
  }
  else{
    waitChildToDie();
  }
}

void extractFileNameInDir(char thepath[PATH_MAX], char extractedName[250][250], int *totalDiscovered){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  int idExtract = 0;
  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
          strcpy(extractedName[idExtract], ep->d_name);
          idExtract+=1;
        }
    }

    (void) closedir (dp);
    printf("There are %d files in %s\n", idExtract, thepath);
  } else perror ("Couldn't open the directory");

  *totalDiscovered = idExtract;
}

void zipFolder(){
  pid_t childAbuse_zip = fork();
  if(childAbuse_zip < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_zip == 0){
    execlp("zip", "zip",  "-rP", "satuduatiga","not_safe_for_wibu.zip", "gacha_gacha", NULL);
  }
  else{
    waitChildToDie();
  }
  
}

void removeDir(){
  pid_t childAbuse_rm = fork();
  if(childAbuse_rm < 0){
    exit(EXIT_FAILURE);
  }
  if(childAbuse_rm == 0){
    // li shi bru, we are in child
    execlp("rm", "rm", "-rf", "gacha_gacha", NULL);
    //here child will die
  }else{
    waitChildToDie(); 
  }
}

void initialize(char fullPathWibu[]){
  // Pertama kali jalan 
  char weaponUrl[100] = "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
  char weaponName[100] = "weapons.zip";
  char weaponLocation[100] ="";
  combineTwoString(weaponLocation, fullPathWibu, weaponName);

  char charaterUrl[100] = "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
  char charaterName[100] ="charaters.zip";
  char characterLocation[100] = "";
  combineTwoString(characterLocation, fullPathWibu, charaterName);
  
  downloadFile(weaponUrl, weaponLocation);
  // waitChildToDie();
  sleep(1);
  downloadFile(charaterUrl, characterLocation);
  // waitChildToDie();
  sleep(1);
  unzipit(characterLocation);
  // waitChildToDie();
  unzipit(weaponLocation);
  // waitChildToDie();
}

void parseJsonRarityName(char fileName[], char nameChar[], char rarityChar[]){
    // printf("calledddddddd\n");
    char *buffer;
    buffer  = malloc(sizeof(char)*16384);

    FILE *fp = fopen(fileName,"r");
    // printf("readin\n");
    fread(buffer, 16384, 1, fp);
    // printf("closin %d\n", res);
    fclose(fp);
    // printf("mamamia lezatos\n");
    struct json_object *parsed_json;
    parsed_json = json_tokener_parse(buffer);
    // printf("ulala syre\n");
    struct json_object *rarity;
    struct json_object *name;
    // printf("naa wae it stop here\n");
    json_object_object_get_ex(parsed_json, "name", &name);
    // printf("ngekstrak name\n");
    json_object_object_get_ex(parsed_json, "rarity", &rarity);


    strcpy(nameChar, json_object_get_string(name));
    strcpy(rarityChar, json_object_get_string(rarity));

    free(buffer);
}

void updateGachaFolderName(char *currentFolderName, int iterGacha){
  // reset the current folder array
  memset(currentFolderName, 0, sizeof(char)*256);
  // get str version of cur iter gacha
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFolderName, "gacha_gacha/total_gacha_");
  strcat(currentFolderName, iterStrGacha);
  strcat(currentFolderName, "/");
  char *command[] = {"-p", currentFolderName, NULL};
  createDir(command);
  free(iterStrGacha);
}

void updateGachaFileName(char *currentFileName, char *currentFolderName, struct tm tmv, int iterGacha){
  memset(currentFileName, 0, sizeof(char)*256);
  //get hour
  char *hours, *minutes, *seconds;
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  hours = malloc(sizeof(char)*4);
  minutes = malloc(sizeof(char)*4);
  seconds = malloc(sizeof(char)*4);
  intToString(tmv.tm_hour, hours, 2);
  intToString(tmv.tm_min, minutes, 2);
  intToString(tmv.tm_sec, seconds, 2);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFileName, currentFolderName);
  // strcat(currentFileName, iterStrGacha);
  // strcat(currentFileName, "/");
  strcat(currentFileName, hours);
  strcat(currentFileName, ":");
  strcat(currentFileName, minutes);
  strcat(currentFileName, ":");
  strcat(currentFileName, seconds);
  strcat(currentFileName, "_gacha_");

  
  strcat(currentFileName, iterStrGacha);
  strcat(currentFileName, ".txt");
  free(hours);free(minutes);free(seconds);free(iterStrGacha);
}
```


```bash
  //get user homedir of whoever running this program
  char *homedir;
  // check ada variable home kah di enviromentnya
  if ((homedir = getenv("HOME")) == NULL) {
    homedir = getpwuid(getuid())->pw_dir;
  }

  char root[100] = "";
  combineTwoString(root, homedir, "/soal1modul2/"); //this is where we do our work
  // printf("%s\n", root);
  if ((chdir(root)) < 0) {
    printf("FAIL\n");
    exit(EXIT_FAILURE);
  }
  // membuat folder gacha gacha di "root"
  char pathGacha[PATH_MAX] = "";
  combineTwoString(pathGacha, root, "gacha_gacha");
  char *gachaName[] = {"-p", pathGacha, NULL};
  createDir(gachaName);

  initialize(root);
```

#### Cara Pengerjaan ####
1. sebelum melakukan download file dilakukan persiapan folder dengan membuat folder `gacha_gacha` di `root` yang digunakan untuk menyimpan hasil gacha.
2. dilakukan pemanggilan fungsi `downloadFile` yang digunakan untuk mendownload folder dari link yang sudah disediakan.
3. proses download dilakukan dengan menggunakan fungsi `excelp(wget)`.
4. selanjutnya akan didapatkan file hasil download dengan format `.zip`.
5. sehingga perlu dilakukan proses ekstrak menggunakn fungsi `unzipit`.
6. dalam proses ekstrak dilakukan dengan menggunakan fungsi `execlp(unzip)` sehingga akan didapatkan 3 folder yakni gacha_gacha,  weapons dan characters dimana folder weapons dan characters berisi file json dan folder gacha_gacha belum ada isinya.

#### Kendala ####


### Soal 1.b ###
Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

#### Dokumentasi Source Code ####
```bash
  srand(time(0));
  int iterGacha = 1;
  int primogems = 79000;
  // FILE *fptr;
  char *currentFileName, *currentFolderName;
  currentFileName = malloc(sizeof(char)*256);
  currentFolderName = malloc(sizeof(char)*256);
  updateGachaFolderName(currentFolderName, iterGacha);
  updateGachaFileName(currentFileName, currentFolderName,tmv, iterGacha); 
  // char digits[12];
  // intToString(123456, digits);
  int isRunning = 1;
  while (isRunning) {
    tmv = getDateTime();
    // ceku taimu
    if(checkDateTime(tmv, 2022, 3, 30, 7, 44) > 0){
      isRunning=0;
      zipFolder();
      removeDir();
      break;
    }
    // Tulis program kalian di sini
    
    // gacha wepon or char
    if(iterGacha&1){
      // this is ganjil ==  character
      //masukin gacha ke txt dengan format 
      //{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}
      int idGachaChar = getRandomNumber(totalChar);
      // printf("randomo aidi: %d\n", idGachaChar);
      char jsonFileName[250];
      char charName[250];
      char rarityChar[3];
      // save the picked random char json file name
      combineTwoString(jsonFileName, "characters/", listChar[idGachaChar]);


      parseJsonRarityName(jsonFileName, charName, rarityChar);

      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_character_%s_%s_%d\n",iterGacha, rarityChar, charName, primogems);
      fclose(fptr);
      printf("saved character on %s\n", currentFileName);
    }else{
      printf("it deez not\n");
      int idGachaChar1 = getRandomNumber(totalWeapon);
      char jsonFileName1[250];
      char charName1[250];
      char rarityChar1[3];

      // save the picked random char json file name
      combineTwoString(jsonFileName1, "weapons/", listWeapon[idGachaChar1]);

      parseJsonRarityName(jsonFileName1, charName1, rarityChar1);

      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_weapon_%s_%s_%d\n",iterGacha, rarityChar1, charName1, primogems);
      fclose(fptr);//ensure every open is lsg close
      printf("saved weapon on %s\n", currentFileName);
    }
    // update new folder or no
    if(iterGacha%90 == 0){
      updateGachaFolderName(currentFolderName, iterGacha);
    }
    // new file or no
    if(iterGacha%10 == 0){
      //update file
      updateGachaFileName(currentFileName, currentFolderName,tmv, iterGacha);

    }
    iterGacha+=1;
    sleep(1);
  }
```

#### Cara Pengerjaan ####
1. Agar hasil random kita gunakan fungsi srand(), dan agar tiap kali di jalankan mengeluarkan hasil ang berbeda, maka kita set seednya sama dengan waktu kode di jalankan srand(time(0));
2. Untuk masalah mod, kita gunakan variable counter yakni iterGacha yang mengkeep track sekarang sudah berapa gacha
3. untuk mendapatkan weapon/character secara acak, pertama tama kita ambil semua nama file hasil extract .zip character dan weapon pada folder character dan weapon dengan meng traverse directory dan menyimpan string pada array. Lalu setelah menyimpen semua nama character dan weaopn dalam array, kita generate random number di antara 0 dan total character atau weapon. Kemudian setelah mendapatkan id, kita bisa akses nama weapon/character lalu kita parse dengan json.
4. Untuk membuat file baru kita gunakan fungsi dari c FILE fptr lalu di fopen dengan mode append
5. Untuk membuat folder baru kita gunakan trio fork, execlp, wait dengan command mkdir bash.

#### Kendala ####


### Soal 1.c ###
Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

#### Dokumentasi Source Code ####
```bash
void updateGachaFolderName(char *currentFolderName, int iterGacha){
  // reset the current folder array
  memset(currentFolderName, 0, sizeof(char)*256);
  // get str version of cur iter gacha
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFolderName, "gacha_gacha/total_gacha_");
  strcat(currentFolderName, iterStrGacha);
  strcat(currentFolderName, "/");
  char *command[] = {"-p", currentFolderName, NULL};
  createDir(command);
  free(iterStrGacha);
}

void updateGachaFileName(char *currentFileName, char *currentFolderName, struct tm tmv, int iterGacha){
  memset(currentFileName, 0, sizeof(char)*256);
  //get hour
  char *hours, *minutes, *seconds;
  char *iterStrGacha;
  iterStrGacha = malloc(sizeof(char)*10);
  hours = malloc(sizeof(char)*4);
  minutes = malloc(sizeof(char)*4);
  seconds = malloc(sizeof(char)*4);
  intToString(tmv.tm_hour, hours, 2);
  intToString(tmv.tm_min, minutes, 2);
  intToString(tmv.tm_sec, seconds, 2);
  intToString(iterGacha, iterStrGacha, -1);
  strcpy(currentFileName, currentFolderName);
  // strcat(currentFileName, iterStrGacha);
  // strcat(currentFileName, "/");
  strcat(currentFileName, hours);
  strcat(currentFileName, ":");
  strcat(currentFileName, minutes);
  strcat(currentFileName, ":");
  strcat(currentFileName, seconds);
  strcat(currentFileName, "_gacha_");

  
  strcat(currentFileName, iterStrGacha);
  strcat(currentFileName, ".txt");
  free(hours);free(minutes);free(seconds);free(iterStrGacha);
}
====================================================
  // get date time
  struct tm tmv = getDateTime();
  // loop sampek 30 maret 2022 04:44
  int isWaiting = 0;
  while(checkDateTime(tmv, 2022, 3, 30, 4, 44) < 0){
    // isWaiting = ;
    printf("waiting: %d", isWaiting);
    tmv = getDateTime();
    printf("now: %d-%02d-%02d %02d:%02d:%02d\n", tmv.tm_year + 1900, tmv.tm_mon + 1, tmv.tm_mday, tmv.tm_hour, tmv.tm_min, tmv.tm_sec);
  }
```

#### Cara Pengerjaan ####
0. Sebelum memulai program, kita cek waktu apakah sudah di atas 30 Maret 04.44
1. Kita dapatkan waktu sekarang dari fungsi getDateTime yang telah dibuat, lalu kita ubah jam, menit, detik ke string, lalu kita gabungkan dan format sesuai soal.
2. Untuk yang memerlukan iter gacha, kita ubah iter gacha ke string lalu gabungkan sesuai format dengan fungsi seperti strcat atau strcpy.

#### Kendala ####


### Soal 1.d ###
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

#### Dokumentasi Source Code ####
```bash
      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_weapon_%s_%s_%d\n",iterGacha, rarityChar1, charName1, primogems);
      fclose(fptr);//ensure every open is lsg close
      printf("saved weapon on %s\n", currentFileName);

      FILE *fptr;
      fptr = fopen(currentFileName,"a");
      primogems-=160;
      fprintf(fptr,"%d_character_%s_%s_%d\n",iterGacha, rarityChar, charName, primogems);
      fclose(fptr);
```

#### Cara Pengerjaan ####
1. Setelah mendapatkan nama file weapon/character, kita buka dan parse dengan json, lalu simpan pada array of char

2.  Kemudian kita buka file txt yang path/namanya disimpan pada array, dan fprintf di text tersebut dengan format sesuai soal

#### Kendala ####


### Soal 1.e ###
Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

#### Dokumentasi Source Code ####
```bash
    if(checkDateTime(tmv, 2022, 3, 30, 7, 44) > 0){
      isRunning=0;
      zipFolder();
      removeDir();
      break;
    }
```

#### Cara Pengerjaan ####
1. dilakukan pengecekan secara berulang untuk mendapatkan waktu ketika jam eksekusi.
2. proses komparasi menggunakan fungsi `strcmp`.
3. jika ditemukan kondisi yang sesuai maka akan dilakukan proses `zip` yang memiliki password dengan menggunakan fungsi `zipFolder` dan memanfaatkan syntax `execlp`.

#### Kendala ####


## Nomor 2 ##
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

### Soal 2.a ###
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

#### Dokumentasi Source Code ####
```bash
    //get user homedir of whoever running this program
    char *homedir;
    // check ada variable home kah di enviromentnya
    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    char root[100] = "";
    combineTwoString(root, homedir, "/shift2/"); //this is where we do our work
    // printf("%s\n", root);
    if ((chdir(root)) < 0) {
        printf("FAIL\n");
        exit(EXIT_FAILURE);
    }

    //  extract drakor.zip
    char savedLoc[256];
    combineTwoString(savedLoc, root, "drakor/");
    // printf("%s\n", savedLoc);
    unzipit("drakor.zip", savedLoc);
    // delete folder, extract category, rename and copy picture
    filterFileDir(savedLoc);
```

#### Cara Pengerjaan ####
1. Pertama -tama kita ganti directory kita ke /home/[usr]/shift2/ dengan chdir
2. Lalu buat folder drakor dengan fungsi createdir yang sudah dibuat dengan trio fork, execlp, wait
3. lalu unzip folder drakor.zip dengan fungsi zipit yang juga dibuat dengan trio fork, execlp, wait
4. kemudian kita traverse directory drakor, dan kemudian deteksi apakah file di sana merupakan folder atau tidak, caranya dengan mengecek apakah ada ekstensi .png di akhirnya, jika tidak kita hapus dengan fungsi remove dir yang juga sudah dibuat dengan trio fork, execlp, wait


#### Kendala ####


### Soal 2.b ###
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

#### Dokumentasi Source Code ####
```bash
void parsePhoto(char photo_name[256], char thepath[PATH_MAX]){
    // create photo path
    char *photoPath;
    photoPath = malloc(sizeof(char)*256);
    combineTwoString(photoPath, thepath, photo_name);
    // variable for parsing
    int isParsing = 1;
    char *filmName, *filmCategory, *filmYear;
    int len = strlen(photo_name);
    int i = 0;
    while(isParsing){
        int isGetName = 1;
        int isGetYear = 0;
        int isGetCategory = 0;
        filmName = malloc(sizeof(char)*128);
        filmCategory = malloc(sizeof(char)*128);
        filmYear = malloc(sizeof(char)*8);
        
        int idName = 0;
        // get name
        for(i = i;i < len && isGetName;i++){
            char c = photo_name[i];
            if(c == ';'){
                isGetName = 0;
                isGetYear = 1;
                filmName[idName] = 0;
            }else{
                filmName[idName] = photo_name[i];
                idName+=1;
            }
        }
        // get year
        int idYear = 0;
        for(i = i;i<len && isGetYear;i++){
            char c = photo_name[i];
            if(c == ';'){
                isGetYear = 0;
                isGetCategory = 1;
                filmYear[idYear] = 0;
            }else{
                filmYear[idYear] = c;
                idYear +=1;
            }
        }
        // get categry
        int idCat = 0;
        for(i = i;i<len && isGetCategory;i++){
            char c = photo_name[i];
            if(c == '_' || c == '.'){
                isGetCategory = 0;
                // filmCategory[idCat] = '/';
                filmCategory[idCat] = 0;
            }else{
                filmCategory[idCat] = c;
                idCat +=1;
            }
        }
        // now let see is there another
        isParsing = photo_name[i-1] == '_';
        // i+= isParsing;
        // save doeloe
        // create dir
        char *categoryPath;
        categoryPath = malloc(sizeof(char)*256);
        combineTwoString(categoryPath, thepath, filmCategory);
        strcat(categoryPath, "/");
        createDir(categoryPath);
        
        // after that we uh save the photo
        // copy then remove
        char *tempPhotoLoc;
        tempPhotoLoc = malloc(sizeof(char)*256);
        combineTwoString(tempPhotoLoc, categoryPath, filmName);
        char *truePhotoLoc;
        truePhotoLoc = malloc(sizeof(char)*256);
        strcpy(truePhotoLoc, tempPhotoLoc);
        strcat(truePhotoLoc, ".png");
        copyFile(photoPath, truePhotoLoc);
        // printf("film name: %s\nfilm year: %s\nfilm cat: %s\ncategory Path: %s\nnew Photo Path: %s\n", filmName, filmYear, filmCategory, categoryPath, newPhotoLoc);
        
        // save the year on txt
        char *textFilePath;
        textFilePath = malloc(sizeof(char)*256);
        strcpy(textFilePath, tempPhotoLoc);
        strcat(textFilePath, ".txt");
        // printf("%s\n", textFilePath);
        writeTextFile(textFilePath, filmName);
        writeTextFile(textFilePath, filmYear);
        free(tempPhotoLoc);free(categoryPath);free(truePhotoLoc);free(textFilePath);
    }
    // remove the photo
    deleteFile(photoPath);
    free(filmCategory);free(filmName);free(filmYear);free(photoPath);
}
```

#### Cara Pengerjaan ####
1. Kita traverse isi directory drakor yang sudah bersih, lalu dari nama file tersebut kita parse dengan fungsi parse photo yang telah dibuat sebelumnya. kita ambil nama, year, category berdasarkan nama file.
2. lalu dari category yang kita dapat, kita buat folder dengan fungsi createdir yang telah kita buat
3. lalu di folder tersebut kita simpan photo dari posternya dengan mengcopy ke folder tersebut sesuai dengan nama yang telah kita parse
4. kita loop sampai semua string telah terparse dan juga semua file terlah terparse
5. tiap kali selesai file di parse, kita hapus file originalnya
6. juga sambil mendapatkan category, kita dapatkan year dan simpan di file txt sementara yang akan kita akses nanti untuk dimasukkan ke data.txt tiap category

#### Kendala ####


### Soal 2.c ###
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

#### Dokumentasi Source Code ####
```bash
Sama dengan di atas
```

#### Cara Pengerjaan ####
1. Sudah di jelaskan pada bagian sebelumnya

#### Kendala ####


### Soal 2.d ###
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.

#### Dokumentasi Source Code ####
```bash
Juga telah dilakukan di atas
```

#### Cara Pengerjaan ####
1. Sama seperti di atas, kita parse nama photo, dan juga telah menghandle kasus lebih dari 1 poster dalam 1 photo

#### Kendala ####


### Soal 2.e ###
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.
kategori : romance

nama : start-up
rilis  : tahun 2020

nama : twenty-one-twenty-five
rilis  : tahun 2022

#### Dokumentasi Source Code ####
```bash
typedef struct
{
    /* data */
    char name[256];
    char year[8];
}movie;
// A function to implement bubble sort
void bubbleSort(movie arr[], int size)
{
    for(int i = 0;i<size;i++){
        for(int j = 0;j<size-i-1;j++){
            // printMovie(arr[i]);
            if(strcmp(arr[j].name, arr[j+1].name) > 0){
                // swap(&arr[j], &arr[j+1]);
                movie temp = arr[j];

                memset(arr[j].name, 0, sizeof(char)*256);
                strcpy(arr[j].name, arr[j+1].name);
                memset(arr[j].year, 0, sizeof(char)*8);
                strcpy(arr[j].year, arr[j+1].year);

                memset(arr[j+1].name, 0, sizeof(char)*256);
                strcpy(arr[j+1].name, temp.name);
                memset(arr[j+1].year, 0, sizeof(char)*8);
                strcpy(arr[j+1].year, temp.year);
            }
        }
    }
}
void compileDataCategory(char thepath[PATH_MAX], char dataFilePath[]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    movie arr_mov[16];
    int idMov = 0;
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
            // not . or ..
            int len = strlen(ep->d_name);
            char format[4] = "txt.";
            int isPng = 4;
            for(int i = 0;i<4;i++){
                // printf("%c==%c\n",ep->d_name[len-i-1],format[i] );
                isPng -= ep->d_name[len-i-1]==format[i];
            }
            
            if(isPng){
                continue;
            }
            // a txt, eh cek dia data tida
            if(strcmp(ep->d_name, "data.txt")==0){
                // skip
                continue;
            }
            char *txtFilePath = malloc(sizeof(char)*256);
            combineTwoString(txtFilePath, thepath, ep->d_name);

            readTextFile(txtFilePath, &arr_mov[idMov]);
            idMov+=1;
            // delete it dud
            deleteFile(txtFilePath);

            free(txtFilePath);
   
            
        }

    }
    // sort it
    bubbleSort(arr_mov, idMov);
    // masukin ke txt
    for(int i = 0;i<idMov;i++){
        char *inputName = malloc(sizeof(char)*256);
        char *inputYear = malloc(sizeof(char)*256);
        combineTwoString(inputName, "nama: ", arr_mov[i].name);
        combineTwoString(inputYear, "rilis : tahun ", arr_mov[i].year);
        writeTextFile(dataFilePath, inputName);
        writeTextFile(dataFilePath, inputYear);
        writeTextFile(dataFilePath, "\n");
        free(inputName);free(inputYear);
    }
    (void) closedir (dp);
  } else perror ("Couldn't open the directory");
  
}
void createDataCategory(char thepath[PATH_MAX]){
  DIR *dp;
  struct dirent *ep;
  char path[100];

  dp = opendir(thepath);
  if (dp != NULL)
  {
    // create data.txt inside
    while ((ep = readdir (dp))) {
        if((ep->d_name[0]) != 46){
            // not . or ..
            int len = strlen(ep->d_name);
            char *categoryPath, *dataCatPath;
            categoryPath = malloc(sizeof(char)*256);
            dataCatPath = malloc(sizeof(char)*256);
            combineTwoString(categoryPath, thepath, ep->d_name);
            strcat(categoryPath, "/");
            combineTwoString(dataCatPath, categoryPath, "data.txt");
            char *header = malloc(sizeof(char)*256);
            combineTwoString(header, "kategori: ", ep->d_name);
            writeTextFile(dataCatPath, header);
            writeTextFile(dataCatPath, "\n");
            compileDataCategory(categoryPath, dataCatPath);
            free(categoryPath);free(dataCatPath);free(header);

            
        }
    }

    (void) closedir (dp);
  } else perror ("Couldn't open the directory");

}
```

#### Cara Pengerjaan ####
1. Kita traverse ke tiap folder kategory dalam drakor
2. kita ambil file txt data nama dan year tiap poster di tiap folder tersebut, lalu simpan ke dalam array of struct movies yang memiliki data nama dan year
3. lalu kita lakukan sorting, disini kami gunakan bubble sort, karena cepat diimplementasi manusia, walau tidak terlalu dalam eksekusi computer. Tetapi karena jumlah poster sedikit, bukanlah suatu masalah.
4. dari array yang telah di sort, kita masukkan ke data.txt untuk tiap categorinya dengan fungsi writeText yang telah dibuat.

#### Kendala ####


## Nomor 3 ##
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

### Soal 3.a ###
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

#### Dokumentasi Source Code ####
```bash
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <dirent.h>

int main() {
    //3a
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/${USER}/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(NULL) != child_id);
    sleep(3)

    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/${USER}/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(NULL) != child_id);
}
```

#### Cara Pengerjaan ####
1. Pertama, digunakan `#include` library yang diperlukan
2. Parent process akan membuat child process dimana dia akan membuat direktori pada `/home/${USER}/modul2/darat` menggunakan perintah `mkdir`. Disini argument `-p` digunakan agar directory yang belum dibuat (seperti `/home/${USER}/modul2`) langsung terbuat.
3. Kemudian parent process akan menunggu `mkdir` selesai dibuat, lalu akan `sleep()` selama 3 detik. 
4. Lalu parent process akan membuat child process lagi untuk membuat direktori pada `/home/${USER}/modul2/air`. 
5. Kemudian parent process akan menunggu child process selesai.

### Soal 3.b ###
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

#### Dokumentasi Source Code ####
```bash
child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "-q", "animal.zip", "-d", "/home/${USER}/modul2", NULL};
        execv("/usr/bin/unzip", argv);
    }
    while(wait(NULL) != child_id);
```

#### Cara Pengerjaan ####
1. Untuk meng-ekstrak file `animal.zip` dapat menggunakan perintah **unzip**.
2. Argument `-q` digunakan agar process **unzip** tidak mengeluarkan output ke terminal.
3. Argument `-d` untuk menyatakan directory output hasil ekstrak.

### Soal 3.c ###
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

#### Cara Pengerjaan ####
1. didefinisikan fungsi yang akan memindahkan dan menghapus file
2. membuka dan mengiterasi isi direktori `/home/${USER}/modul2/animal` 
3. jika nama file mengandung substring darat, maka akan dipindahkan ke direktori darat
4. jika nama file mengandung substring air, maka akan dipindahkan ke direktori air
5. selain itu akan dihapus

#### Kendala ####
fungsi yang digunakan untuk memindahkan file belum terbuat

### Soal 3.d ###
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

#### Cara Pengerjaan ####
1. membuka dan mengiterasi isi direktori home/${USER}/modul2/darat
2. apabila nama file mengandung substring `bird`, maka akan dihapus

#### Kendala ####
fungsi yang digunakan untuk menghapus file belum terbuat

### Soal 3.e ###
e.	Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

#### Cara Pengerjaan ####
1. membuat variabel yang memuat destinasi dari folder dan juga membuat `list.txt`
2. membuka dengan menggunakan `fopen`
3. karena dibutuhkan permission dari user, maka harus mendapatkan permission menggunakan `pwuid` dari modul2 yang ada di github
4. copy r, w, x dan cetak sebagai nama dari file di `list.txt`

#### Kendala ####
belum bisa mengimplementasikannya ke bentuk kode

## Dokumentasi ##


