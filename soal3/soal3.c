#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <dirent.h>

int main() {
    //3a
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/${USER}/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(NULL) != child_id);
    sleep(3)

    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/home/${USER}/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(NULL) != child_id);

    //3b
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "-q", "animal.zip", "-d", "/home/${USER}/modul2", NULL};
        execv("/usr/bin/unzip", argv);
    }
    while(wait(NULL) != child_id);

}

